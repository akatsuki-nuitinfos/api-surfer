#! /bin/bash

./gradlew clean build -x test

JAR_FILE=$(ls build/libs/ | grep "^akatsuki")
echo $JAR_FILE

docker build . --build-arg jar=build/libs/$JAR_FILE -t ezzefiohez/nuitinfos-principale
docker push ezzefiohez/nuitinfos-principale


curl  -X POST http://146.59.248.65:9000/api/webhooks/e40cbb7d-a092-4c66-93f8-58892e1e7375
