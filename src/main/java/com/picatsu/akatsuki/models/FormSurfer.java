package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "formSurfer")
@Builder
public class FormSurfer {

    @Id
    private String id;

    private String ville;
    private Date debut;
    private Date fin;
    private Spot spot;
    private Waterman waterman;
    private String userId;


}
