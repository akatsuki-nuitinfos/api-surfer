package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Spot {

    private String nom;
    private int baigneurs;
    private int pratiquand_activite;
    private int nbBateauxPeche;
    private int nbBateauxLoisir;
    private int nbBateauxAvoile;

}
