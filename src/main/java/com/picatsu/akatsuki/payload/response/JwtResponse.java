package com.picatsu.akatsuki.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String id;
	private String username;
	private String firstName;
	private String lastName;
	private String adresse;
	private String email;
	private List<String> roles;

	public JwtResponse(String accessToken, String id, String username, String email, String firstName, String lastName, String adresse, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.firstName = firstName;
		this.lastName = lastName;
		this.adresse = adresse;
	}


}
