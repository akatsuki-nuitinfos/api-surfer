package com.picatsu.akatsuki.controllers;



import com.picatsu.akatsuki.models.FormSurfer;
import com.picatsu.akatsuki.repository.FormSurferRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;


@RequestMapping(value = "/api/v1/surfers")
@RestController("FormSurferController")
@Slf4j
@CrossOrigin
public class FormSurferController {

    @Autowired
    private FormSurferRepository formSurferRepository;


    @GetMapping(value = "/all")
    public List<FormSurfer> getAll(){
        log.info("Get all FormSurfer ");


        return formSurferRepository.findAll();
    }

    @GetMapping(value = "/weather/{ville}")
    public Mono<Object> getWeather(@PathVariable(value= "ville") String ville){
        log.info("Get all FormSurfer ");


        String uri = "http://api.weatherstack.com/current?access_key=84d872f0e888e32b68d60680c23edd48&query=" + ville;


        WebClient webClient = WebClient.create();

        return webClient.get()
                .uri(uri)
                .retrieve()
                .bodyToMono(Object.class);

    }

    @GetMapping(value = "/weather/plages")
    public String[] getWeatherPlages(){

        log.info("Get all Plages Surfers ");

        String[] strArray2 = {"HOSSEGOR","SEIGNOSSE","SAINT%20JEAN%20DE%20LUZ", "PLOMEUR", "ANGLET", "LACANAU", "GUÉTHARY", "QUIBERON", "ÎLE%20D’OLÉRON"  };

        return strArray2;

    }

    @GetMapping(value = "/{userId}")
    public List<FormSurfer> getMyFormsAll(@PathVariable(value= "id") String id){
        log.info("Get all FormSurfer ");

        return formSurferRepository.findAllByUserId(id);
    }

    @PostMapping(value = "/add")
    public FormSurfer addFormSurfer(@RequestBody FormSurfer t) {

        log.info("Add FormSurfer " + t.toString());

        return formSurferRepository.save(t);
    }

    @PutMapping(value = "/update")
    public FormSurfer updateFormSurfer(@RequestBody FormSurfer t) {

        log.info("update FormSurfer " + t.toString());

        if( formSurferRepository.findById(t.getId()) != null ) {
            formSurferRepository.deleteById(t.getId());
            return formSurferRepository.save(t);
        }

        return formSurferRepository.findById(t.getId()).get();
    }

    @DeleteMapping(value= "/{id}")
    @Operation(summary = "delete FormSurfer from db")
    public ResponseEntity<?> deleteFormSurfer(@PathVariable(value= "id") String id) {
        log.info("deleting FormSurfer :  " + id);

        long val =  formSurferRepository.deleteAllById(id);

        if ( val == 1) {
            return new ResponseEntity<>("Deleted successfully ", HttpStatus.OK);
        }
        if( val == 0 ) {
            return new ResponseEntity<>("Cannot find Symbol : " + id, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>("Obscure error ", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping(value= "/all")
    @Operation(summary = "delete ALL FormSurfer from db")
    public ResponseEntity<?> deleteAllFormSurfer() {

        log.info("deleting ALL FormSurfer :  " );

        formSurferRepository.deleteAll();

        return new ResponseEntity<>("Done ", HttpStatus.ACCEPTED);
    }
}
