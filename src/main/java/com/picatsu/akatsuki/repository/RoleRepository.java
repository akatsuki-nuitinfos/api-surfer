package com.picatsu.akatsuki.repository;

import java.util.Optional;

import com.picatsu.akatsuki.models.ERole;
import com.picatsu.akatsuki.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface RoleRepository extends MongoRepository<Role, String> {
  Optional<Role> findByName(ERole name);

  @Override
  <S extends Role> S save(S s);
}
