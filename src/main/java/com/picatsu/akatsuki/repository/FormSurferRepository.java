package com.picatsu.akatsuki.repository;

import com.picatsu.akatsuki.models.FormSurfer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface FormSurferRepository extends MongoRepository<FormSurfer, String> {

    long deleteAllById(String id);

    List<FormSurfer> findAllByUserId(String userId);
}
